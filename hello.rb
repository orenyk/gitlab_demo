require 'sinatra'

get '/' do
  "Hello Yale!"
end

get '/foo' do
  "This is tested"
end

get "/foo/:id" do
  "Foo is number #{params['id']}"
end

get "/bar" do
  "Bar is here"
end

get "/baz" do
  "Here is baz"
end
