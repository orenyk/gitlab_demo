require "spec_helper"

describe "/foo", type: :feature do
  it "works" do
    visit "/foo"
    expect(page).to have_content("This is tested")
  end
end

describe "/foo/:id", type: :feature do
  it "works" do
    visit "/foo/2"
    expect(page).to have_content("Foo is number 2")
  end
end
