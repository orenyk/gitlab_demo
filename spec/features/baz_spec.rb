require "spec_helper"

describe "/baz", type: :feature do
  it "works" do
    visit "/baz"
    expect(page).to have_content("Here is baz")
  end
end
